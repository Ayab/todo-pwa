/**
 * 
 */
export function fetchTodo() {
  const config = window.config;

  return fetch(`${config.api}/todos`, {
    method: 'GET',
    headers: {
      'Content-type': 'application/json'
    }
  })
  .then(result => result.json())
  .catch(error => {
    console.error(error);
    return false;
  })
}
