import page from 'page';
import { fetchTodo } from './api/todo.js';
import { setTodos } from './idb.js';

const app  = document.querySelector('#app .outlet');

fetch('/config.json')
  .then((result) => result.json())
  .then(async (config) => {
    console.log('[todo] Config loaded !!!');
    window.config = config;

    page('/', async () => {

      let todos = [];
      if (navigator.onLine) {
        const data = await fetchTodo();
        todos = await setTodos(data); 
      } else {
        todos = await getTodos() || [];
      }

    });

    page();
  });